function values(obj) {
    const value=[]
    for (let keys in obj){
        if(typeof obj[keys]!=='function'){
            value.push(obj[keys])
        }
    }
    return value
}

module.exports=values