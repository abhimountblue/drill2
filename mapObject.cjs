const { default: values } = require("./values.cjs")

function mapObject(obj, cb=(values)=>values) {
  const obj2 ={}

    for(let key in obj ){

        if(typeof obj[key]!='number'){
            obj2[key]=obj[key]
        }else{
        obj2[key]=cb(obj[key])
        }
    }
    return obj2
}

module.exports=mapObject